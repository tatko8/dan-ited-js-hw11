let password = document.querySelectorAll(".icon-password");

function switchPassword(event) {
  let input = event.target.previousElementSibling;
  let icon = event.target;

  if (input.type === "password") {
    input.type = "text";
    icon.classList.remove("fa-eye");
    icon.classList.add("fa-eye-slash");
  } else {
    input.type = "password";
    icon.classList.remove("fa-eye-slash");
    icon.classList.add("fa-eye");
  }
};

password.forEach((icon) => {
  icon.addEventListener("click", switchPassword);
});

// порівняти введені значення в полях паролів 
let button = document.querySelector(".btn");
let inputPassword = document.querySelectorAll("input[type='password']");

function comparePassword(event) {
  event.preventDefault(); 
  if (inputPassword[0].value === inputPassword [1].value) {
    alert("You are welcome");
  } else {
    let p = document.createElement("p");
    p.textContent = "Потрібно ввести однакові значення";
    p.style.color = "red";
    button.parentNode.insertBefore(p, button);
  }
};

button.addEventListener("click", comparePassword);
